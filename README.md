# URL Checker
URL Checker is meant to be used along side a proxy service to determine if a given URL is safe or not. The URL Checker service will provide a simple response along with an HTTP status code for a given request. Docker compose is used for the local development, and will make getting this application runing quick and easy.
For this project, the backend datastore redis was chosen primarily because of it's speed. URL Checker is intended to be used in a proxy service that will block a user's request until a response is given. With that in mind, a very fast data store will be able to handle a large number of user requests at a time without creating a noticable lag. Redis also supports replication for redundancy. Redis Sentinel supports promoting replicas to masters in the case of failure as well.

Simplicity is a very strong motivator behind the design decisions in this application. By using HTTP status codes, interfacing with other systems is made even easier. The JSON responses don't even need to be parsed, as HTTP status codes provide enough information for an external service to determine the result.

Flask was chosen as the python framework in this project, specifically `flask-restful`, providing a more object oriented take on Flask. The reason for this is the simplicity with which flask allows access to various parts of an HTTP request. Many other API frameworks do fantastic jobs of parsing path and query parameters but can make it more difficult to get the entire URI to work with. This project parses parameters in a slightly different way than most other APIs in that it just ignores most of the parameters with the intention of adding the entire passed URL to the database. This is in contrast to many other APIs that will parse all of the parameters passed.

## Service Use
### Get
Once the service is running a user (or other service) can check if a URL is valid by sending an `HTTP GET` request to the endpoint `http://<server url>/urlinfo/1/example.com/something?query=here` where `example.com/something?query=here` is the URL in question. The service will respond with HTTP 200 along with a message `{"url": url, "status": "found"}` where `url` is the URL sent to the service, when the URL is found, indicating there is malware at the URL. The service will respond with HTTP 404 and a message `{"url": url, "status": "not found"}` indicating the URL has not been located in the malware database.

> An example of a get request (with the service running on the `localhost`):
```
curl -XGET "http://127.0.0.1:5000/urlinfo/1/example.com/something?query=here
```
> An example response when the URL is found (HTTP 200):
```
{"url": "example.com/something?query=here", "status": "found"}
```
> And example response when the URL is not found (HTTP 404):
```
{"url": "example.com/something?query=here", "status": "not found"}
```

### Put
To add URLs to the malware database a user (or proxy service) can send an `HTTP PUT` request, similar to the GET request.
> An example of a put request (with the service running on the `localhost`):
```
curl -XPUT "http://127.0.0.1:5000/urlinfo/1/example.com/something?query=here
```
> An example response when the URL is already exists in the malware database (HTTP 200):
```
{"url": "example.com/something?query=here", "status": "exists"}
```
> And example response when the URL is does not exist and so is added (HTTP 201):
```
{"url": "example.com/something?query=here", "status": "added"}
```

### Delete
URL Checker also supports deleting URLs in the case that a given URL is found to no longer contain malware. Similar to `GET` and `PUT`:
> An example of a delete request (with the service running on the `localhost`):
```
curl -XDELETE "http://127.0.0.1:5000/urlinfo/1/example.com/something?query=here
```
> An example response when the URL is already exists in the malware database (HTTP 200):
```
{"url": "example.com/something?query=here", "status": "exists"}
```
> And example response when the URL is does not exist and so is added (HTTP 201):
```
{"url": "example.com/something?query=here", "status": "added"}
```

## Running the code (in development environments)
*The code provided in this repo includes instructions on running the code, but is meant for a development environment. The below section (Part 2 - Short Answer Quesitons) will discuss steps that are recommended should this application be run in a production environment.*
The only dependency to run this on your local computer is to have [docker](https://www.docker.com/get-started) installed.

#### With Docker
With docker installed, the `docker compose` command is available. For context, `docker compose` allows running multiple containerized services with dependencies and configuration locally.
To start, run:
```
docker compose up
```
This will pull down a redis container and start it before locally building the application's docker container based on python 3.9.7. Once redis is started and URL Checker's container is built, URL Checker is started listening on port `5000` (`http://127.0.0.1:5000`).

Should any changes be made to the Dockerfile, or python code, simply stop the containers with `docker compose down`, rebuild the container with `docker compose build`, and start the containers back up with `docker compose up`.

#### Without Docker
While using docker is the recommended method of running this application, since no redis setup or configuration is necessary, it can be run locally. It is necessary to have another instance of redis that can be used. Simply set the environment variables associate with redis, to ensure URL Checker can find redis:
```
REDIS_HOST
REDIS_PORT
REDIS_PASS
```
> It is beyond the scope of this article to set up a local instance of redis, but for a hint you can always use `docker run -d -p 6379:6379 redis`, but if you're doing that, why not just run the whole thing using `docker compose`?

### Tests
This project includes a number of tests to speed up the development cycle while exposing any breaking changes (ideally ;) ). The tests are built with `pytest`, one of the most popular python based testing frameworks.
#### With Docker
To run the tests using docker compose simply run:
```
docker compose run tests
```
> It's worth pointing out that if any changes have been made to any python files, including tests, the container images will need to be rebuilt with `docker compose build`

To simluate redis for testing the python module [`fakeredis`](https://github.com/jamesls/fakeredis) was used.

#### Without Docker
These tests can also be run locally without the redis dependency that running the whole application had. This could be useful to shorten the feedback cycle because a docker conatiner doesn't need to be rebuilt every time.
> Simply make sure the python requirements are installed locally:
```
pip install -r requirements.txt
```
> Then run the tests from this directory
```
pytest -v
```

## Running the code (IN PRODUCTION!)
The code here was designed to run in a local development environment and the following steps need to be taken in the case it will need to be run in production:
  1. **Change / set the secret key**. This application is set to run in development mode, which will utilize a default key for tasks such as creating session cookies, and others. Before putting this application in production a production key should be generated. [The flask documentation has instructions on generating a new secret key](https://flask.palletsprojects.com/en/1.1.x/tutorial/deploy/#configure-the-secret-key). A reminder that the production key should not be kept in version control due to the sensitive nature of the key.

  2. **Use a production WSGI server**. Because this application is running in development mode, it is using a local, built in web server. This web server is handy for local development but is not suitable for larger amounts of traffic. Any WSGI server can be used, but [uWSGI with nginx as a proxy](https://flask.palletsprojects.com/en/1.1.x/deploying/uwsgi/) is the recommended approach.

  3. **Use an external Redis instance**. The instance of Redis included with this project is meant to run locally. For production, an external instance of Redis should be used. The application can be configured to use any external Redis instance by setting the environment variables `REDIS_HOST`, `REDIS_PASS` and `REDIS_PORT`. Also, because Redis is an in-memory data-store it's worth mentioning that in production Redis should persist the data to disk, in the case the server or process died unexpectedly.

  4. **Container orchestration**. This application can be deployed using any method that is desired, but the recommended approach is to use the included `Dockerfile`, and run the application in a container orchestration platform, such as Kubernetes. In that case, following best practices, the container should be built and pushed to an external container image registry, then a Kubernetes deployment and service should be created for this application.


## Part 2 - Short Answer Questions:
* The size of the URL list could grow infinitely. How might you scale this beyond the memory capacity of the system? 
Because redis is used as the backend datastore here, scaling based on the dataset would be a matter of scaling the redis system, or cluster. Redis does support clustering, but ultimately is limited by the resources of the redis server. Should the dataset continue to grow it would be worth examining a more robust clustered database, such as mongoDB or CasandraDB, both of which support sharding for very large datasets, but also enjoy the benefits of a non-relational datastore. Another option is utilizing redis master-master replication. This is only available with Redis Enterprise.

* Assume that the number of requests will exceed the capacity of a single system, describe how might you solve this, and how might this change if you have to distribute this workload to an additional region, such as Europe. 
URL Checker was designed to be stateless, so easily scales to multiple application nodes, sharing a database. Because redis is utilized to not only store the data but to determine the presence of a URL, the stateless application layer URL Checker should be very performant under load.
However, if this appliation is to be distributed to an additional region, such as Europe, consistency may become an issue. In that case, either a solution like [Netflix's dynomite](https://github.com/Netflix/dynomite/wiki/Topology), Redis' master-master replication, or the use of another datastore (e.g. MongoDB or CasandraDB as mentioned in the last answer) can be considered.

* What are some strategies you might use to update the service with new URLs? Updates may be as much as 5 thousand URLs a day with updates arriving every 10 minutes.
The service supports using HTTP PUT to add a single URL, but this would not be ideal for large batches. In that case, adding a new method to the `UrlChecker` class to for bulk adds would be useful. This could allow a list of URLs to be passed in the request body instead of using the endpoint itself. The request to this additional method might look like:
```
curl -XPUT -d '{"url": ["http://badurl1.com", "http://theresmalwarehere.com", "http://etc.com"]}' 'http://<address of service>/urlinfo/1/bulkadd'
```

* [On-Call] You’re woken up at 3am, what are some of the things you’ll look for? Does that change anything you’ve done in the app?
If I was woken up at 3am I would first look at the service logs. URL Checker will print all messages to `stdout` and `stderr`, which are easily viewable with `docker` or other container orchestration platforms, such as Kubernetes. If there weren't issue with the application I would look at the datastore, Redis. If it was a single server I would see if there were any general errors in the logs. I would verify the system Redis was running on had enough memory and disk space. If Redis was clustered I would verify the cluster was still in tact and that there hadn't been any unintended promotions from replica to master (if using Redis Sentinel).
As far as whether that would change anything in the app, I might look at a more partition tolerant data store, like Cassandra or MongoDB.

* What are some considerations for the lifecycle of the app?
In order to ensure the application is running optimally, observability will be an important part of running this application in production. The application will output to `stdout` and `stderr` like any good containerized application, so there should be a solution available to view current and past logs should the need arise to spot certain trends. Collecting metrics would also be helpful in understanding where there is room for improvement. Metrics that come to mind are numebr of requests per second being processed and time the proxy is blocking in waiting for a response from this application. It would also be useful to examine trends in malware URLs being added, but that's a bit beyond the scope of this article.

As far as CI/CD, the tests should be run every time there is a pull request for a change to this code, ensuring the PR doesn't introduce a breaking change should it be merged and deployed.

* You need to deploy a new version of this application. What would you do?
I would make my change to a feature branch and submit a pull request. I would have run the tests locally, to ensure my change doesn't have any unintended consequences. Ideally, I'd have at least one other person review the change to spot any issues I may have missed. Assuming that other person approved my pull request, my change would be merge to the master branch of this repo. Assuming there is automated CI/CD based on merges to the master branch, the process would be triggered by the merge to master. A new container would be built with a unique tag, as release artifacts should be immutable, and the docker image, in this case, is the release artifact. The container image would be built and pushed to the (private) container registry. From there a CI/CD pipeline would hopefully deploy a new release to the container orchestration platform. In the case of Kubernetes, a new version would be deployed as a replica set, and once any health and liveness checks passed traffic would be diverted to the new containers, and the old ones would be terminated.

Because there is no relational database, schema changes should not be needed, so there should be zero downtime between any deployments.
