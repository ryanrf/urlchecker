FROM python:3.9.7-slim
RUN useradd -md /app user
USER user
COPY app.py requirements.txt UrlChecker.py test_app.py /app/
WORKDIR /app
RUN pip install -r requirements.txt
CMD ["/app/.local/bin/flask", "run", "--host=0.0.0.0"]