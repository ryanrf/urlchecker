from flask import Flask
from flask_restful import Api
from UrlChecker import UrlChecker
from os import environ


redis_host = environ.get('REDIS_HOST', '127.0.0.1')
redis_port = environ.get('REDIS_PORT', '6379')
redis_pass = environ.get('REDIS_PASS', '')

def create_app(testing=False):
    app = Flask(__name__)
    api = Api(app)
    app.config['TESTING'] = testing
    if not app.config['TESTING']:
        from redis import Redis
        r = Redis(
            host=redis_host,
            port=redis_port, 
            password=redis_pass)
        app.logger.debug(f"Redis connection initiated to {redis_host} on port {redis_port}")
    else:
        import fakeredis
        r = fakeredis.FakeStrictRedis()
        app.logger.debug(f"Redis connection mocked for testing")
    checker = UrlChecker.setUp(app=app, redis=r)
    api.add_resource(checker, '/urlinfo/1/<path:path>')
    return app
    
if __name__ == "__main__":
    app = create_app()
    app.run(debug=True)