from app import create_app
import pytest

url = "example.com/an_endpoint?query=True"
endpoint = "/urlinfo/1/" + url

@pytest.fixture
def client():
    app = create_app(testing=True)
    with app.app_context():
        with app.test_client() as client:
            yield client
               
def test_put_url(client):
    response = client.put(endpoint)
    assert response.status_code == 201
    assert response.get_json() == {"url" : url, "status": "added"}

def test_get_good_url(client):
    client.put(endpoint)
    response = client.get(endpoint)
    assert response.status_code == 200
    assert response.get_json() == {"url": url, "status": "found"}
    
def test_delete_url(client):
    client.put(endpoint)
    response = client.delete(endpoint)
    print(response.get_json())
    assert response.status_code == 201
    assert  response.get_json() == {"url": url, "status": "deleted"}

def test_get_bad_url(client):
    response = client.get(endpoint + "?query2=yup")
    assert response.status_code == 404
    assert response.get_json() == {"url": url + "?query2=yup", "status": "not found"}
    
def test_delete_bad_url(client):
    response = client.delete(endpoint)
    assert response.status_code == 404
    assert response.get_json() == {"url": url, "status": "not found"}
