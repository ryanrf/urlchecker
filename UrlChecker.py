from flask import Flask, request
from flask_restful import Resource
from redis import Redis
from re import split as re_split

class UrlChecker(Resource):
    @classmethod
    def setUp(cls, app: Flask, redis: Redis):
        cls.redis = redis
        cls.app = app
        return cls
    
    def clean_path(self, path):
        path = path.rstrip('/')
        parts = path.split('/')
        host = parts[0]
        rpath_parts = re_split(host, request.full_path)
        rpath = rpath_parts[-1] if len(rpath_parts) > 1 else None
        self.app.logger.debug(f'host = {host}')
        self.app.logger.debug(f'params = {rpath}')
        return host, rpath, host + rpath
    
    def get(self, path: str):
        """ Get a url, responding with appropriate HTTP response
        Along with json status message"""
        host, rpath, url = self.clean_path(path)
        if self.redis.sismember(host, rpath):
            self.app.logger.info(f"url {url} found to contain malware")
            return {"url": url, "status": "found"}, 200
        else:
            self.app.logger.info(f"{url} not found in malware database")
            return {"url": url, "status": "not found"}, 404
    

    def put(self, path: str):
        """ Add a URL to database, responding with appropriate HTTP status codes
        to indicate if URL already exists, or needed to be created
        Using PUT for idempotency"""
        host, rpath, url = self.clean_path(path)
        if self.redis.sismember(host, rpath):
            self.app.logger.info(f'Found full URL {url} in datastore')
            return {"url": url, "status": "exists"}, 200
        else:
            self.app.logger.info(f"{url} not found in datastore. Adding...")
            self.redis.sadd(host, rpath)
            return {"url": url , "status": "added"}, 201
    
    def delete(self, path: str):
        """ Delete a URL
        if params are not passed all urls associated with the host are removed"""
        host, rpath, url = self.clean_path(path)
        if rpath:
            exist = self.redis.srem(host, rpath)
        else:
            exist = self.redis.delete(host)
        if exist:
            self.app.logger.info(f"Successfully deleted {url}")
            return {"url": url, "status": "deleted"}, 201
        else:
            self.app.logger.info(f"{path} not found. Nothing to delete")
            return {"url": url, "status": "not found"}, 404
